<?php 
/*
	This is the include file to go along with the media posting app for the iphone. It's 
	pretty generic so it can be used in a number of situations.
	
	Author: Claude Sutterlin
	Date: 6/8/2012
*/

class UploadedFile{
	public $postedFile;
	public $filePath;
	public $thumbPath;
	public $altPath;
	public $mediaType;
	public $name;
	public $settings = array();
	
	public function __construct($postedFile, $l_settings){
		$this->postedFile = $postedFile;
		
		$this->name = uniqid() . $this->postedFile['name'];
		$this->settings = $l_settings; 
	}
	
	public function processUpload(){
		$file = $this->postedFile;
		
		$fileName = $this->name;
		$path = $this->settings['uploadFolder'].$fileName;
		$thumbpath = $this->settings['thumbFolder'].$fileName;
		$altpath = $this->settings['altFolder'].$fileName;
			
		//copy the file in
		if (copy($file['tmp_name'], $path)){ //IF IT HAS BEEN COPIED... 
			
			//store the uploaded file path
			$this->filePath=$path;	
				
			//see if the file is a video
			if (substr_count(strtolower($path), ".mov")){
				$this->mediaType = 'video';
				
				if ($this->settings['createThumb']){
					$thumbPath = $thumbFolder."videothumb.png";
				}
			}
			//its a picture
			else{	
				$this->mediaType = 'image';
				
				if ($this->settings['createAlt']){
					resize_image_to($path, $altpath, 400);
					$this->altPath=$altpath;
				}
				
				if ($this->settings['createThumb']){
					create_square_image($path,$thumbpath,75);
					$this->thumbPath=$thumbpath;
				}
			}
			
			//it has been uploaded
			return true;
		}
	}
}

class Upload{
	public $success = false;
	public $filecount = 0;
	public $files = array();
	
	/* Default Settings */
	public $settings = array();
	
	public function __construct($l_settings){
		$this->settings['uploadFolder'] = "uploads/";
			
			//settings for thumbnail creation
		$this->settings['createThumb'] = true;
		$this->settings['thumbFolder'] = "uploads/thumbs/";
		$this->settings['thumbSize'] = 75;
						
			//alternate size image creation
		$this->settings['createAlt'] = true;
		$this->settings['altFolder'] = "uploads/alt/";
		$this->settings['altSize'] = 400;
			
			//what folder to save uploads into
					if (isSet($l_settings['uploadFolder'])){
						$this->settings['uploadFolder'] = $l_settings['uploadFolder'];
					}
						
					//settings for thumbnail creation
					if (isSet($l_settings['createThumb'])){
						$this->settings['createThumb'] = $l_settings['createThumb'];
					}
					
					if (isSet($l_settings['thumbFolder'])){
						$this->settings['thumbFolder'] = $l_settings['thumbFolder'];
					}
					
					if (isSet($l_settings['thumbSize'])){
						$this->settings['thumbSize'] = $l_settings['thumbSize'];
					}
						
					//alternate size image creation
					if (isSet($l_settings['createAlt'])){
						$this->settings['createAlt'] = $l_settings['createAlt'];
					}
					
					if (isSet($l_settings['altFolder'])){
						$this->settings['altFolder'] = $l_settings['altFolder'];
					}
					
					if (isSet($l_settings['altSize'])){
						$this->settings['altSize'] = $l_settings['altSize'];
					}
					
	}
	
	
	function processUploads(){
		//see how many files we have
		$fileCount = 0;
		if (isSet($_POST['fileCount'])){
			$fileCount = $_POST['fileCount'];
		}
		
		$filesCompleted=0;
		//loop over each file
		for ($i=0; $i<$fileCount; $i++){
		
			//get the file
			$postedName = 'uploadedFile' . $i;			
			$file = $_FILES[$postedName];
			
			//create the file
			$f = new UploadedFile($file, $this->settings);
			
			//process it 
			if ($f->processUpload()){
				$this->files[$filesCompleted] = $f;
				$filesCompleted++;
			}
		}

		$this->filecount = $filesCompleted;
		
		//see if the number of files sent = number of files downloaded
		// output simple string so the app knows the status
		if (($fileCount==0)||($this->filecount != $filesCompleted)){
			echo "Failed";
			$this->success =  false;
		}
		else{
			echo "Success";
			$this->success = true;
		}
	}
}

	function resize_image_to($file, $tofile, $max_size = 200){
		// get width and height of original image
		$imagedata = getimagesize($file);
		$original_width = $imagedata[0];	
		$original_height = $imagedata[1];
		
		if($original_width > $original_height){
			$new_height = $max_size;
			$new_width = $new_height*($original_width/$original_height);
		}
		if($original_height > $original_width){
			$new_width = $max_size;
			$new_height = $new_width*($original_height/$original_width);
		}
		
		$new_width = round($new_width);
		$new_height = round($new_height);
		
		// load the image
		if(substr_count(strtolower($file), ".jpg") or substr_count(strtolower($file), ".jpeg")){
			$original_image = imagecreatefromjpeg($file);
		}
		if(substr_count(strtolower($file), ".gif")){
			$original_image = imagecreatefromgif($file);
		}
		if(substr_count(strtolower($file), ".png")){
			$original_image = imagecreatefrompng($file);
		}
		
		$smaller_image = imagecreatetruecolor($new_width, $new_height);
		
		imagecopyresampled($smaller_image, $original_image, 0, 0, 0, 0, $new_width, $new_height, $original_width, $original_height);
		
		// save the smaller image FILE if destination file given
		if(substr_count(strtolower($file), ".jpg")){
			imagejpeg($smaller_image,$tofile,100);
		}
		if(substr_count(strtolower($file), ".gif")){
			imagegif($smaller_image,$tofile);
		}
		if(substr_count(strtolower($file), ".png")){
			imagepng($smaller_image,$tofile,9);
		}

		imagedestroy($original_image);
		imagedestroy($smaller_image);
		
	}

	function create_square_image($original_file, $destination_file=NULL, $square_size = 96){		
		//if(isset($destination_file) and $destination_file!=NULL){
		//	if(!is_writable($destination_file)){
	//			echo '<p style="color:#FF0000">Oops, the destination path is not writable. Make that file or its parent folder wirtable.</p>'; 
	//		}
	//	}
		
		// get width and height of original image
		$imagedata = getimagesize($original_file);
		$original_width = $imagedata[0];	
		$original_height = $imagedata[1];
		
		if($original_width > $original_height){
			$new_height = $square_size;
			$new_width = $new_height*($original_width/$original_height);
		}
		if($original_height > $original_width){
			$new_width = $square_size;
			$new_height = $new_width*($original_height/$original_width);
		}
		if($original_height == $original_width){
			$new_width = $square_size;
			$new_height = $square_size;
		}
		
		$new_width = round($new_width);
		$new_height = round($new_height);
		
		// load the image
		if(substr_count(strtolower($original_file), ".jpg") or substr_count(strtolower($original_file), ".jpeg")){
			$original_image = imagecreatefromjpeg($original_file);
		}
		if(substr_count(strtolower($original_file), ".gif")){
			$original_image = imagecreatefromgif($original_file);
		}
		if(substr_count(strtolower($original_file), ".png")){
			$original_image = imagecreatefrompng($original_file);
		}
		
		$smaller_image = imagecreatetruecolor($new_width, $new_height);
		$square_image = imagecreatetruecolor($square_size, $square_size);
		
		imagecopyresampled($smaller_image, $original_image, 0, 0, 0, 0, $new_width, $new_height, $original_width, $original_height);
		
		if($new_width>$new_height){
			$difference = $new_width-$new_height;
			$half_difference =  round($difference/2);
			imagecopyresampled($square_image, $smaller_image, 0-$half_difference+1, 0, 0, 0, $square_size+$difference, $square_size, $new_width, $new_height);
		}
		if($new_height>$new_width){
			$difference = $new_height-$new_width;
			$half_difference =  round($difference/2);
			imagecopyresampled($square_image, $smaller_image, 0, 0-$half_difference+1, 0, 0, $square_size, $square_size+$difference, $new_width, $new_height);
		}
		if($new_height == $new_width){
			imagecopyresampled($square_image, $smaller_image, 0, 0, 0, 0, $square_size, $square_size, $new_width, $new_height);
		}
		

		// if no destination file was given then display a png		
		if(!$destination_file){
			imagepng($square_image,NULL,9);
		}
		
		// save the smaller image FILE if destination file given
		if(substr_count(strtolower($destination_file), ".jpg")){
			imagejpeg($square_image,$destination_file,100);
		}
		if(substr_count(strtolower($destination_file), ".gif")){
			imagegif($square_image,$destination_file);
		}
		if(substr_count(strtolower($destination_file), ".png")){
			imagepng($square_image,$destination_file,9);
		}

		imagedestroy($original_image);
		imagedestroy($smaller_image);
		imagedestroy($square_image);

	}

?>