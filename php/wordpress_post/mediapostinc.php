<?php 
/*
	This is the include file to go along with the media posting app for the iphone. It's 
	pretty generic so it can be used in a number of situations.
	
	Author: Claude Sutterlin
	Date: 6/8/2012
*/


//main upload
public $uploadFolder = "uploads/"; 
	
//settings for thumbnail creation
public $createThumb = true;
public $thumbFolder = "uploads/thumbs/";
public $thumbSize = 75;
	
//alternate size image creation
public $createAlt = true;
public $altFolder = "uploads/alt/";
public $altSize = 400;
	

class UploadedFile{
	public $postedFile;
	public $filePath;
	public $thumbPath;
	public $altPath;
	public $mediaType;
	public $name;
	
	public function __construct($postedFile){
		$this->postedFile = $postedFile;
		
		$this->name = uniqid() . $this->postedFile['name']; 
	}
	
	public function processUpload(){
		$file = $this->postedFile;
		
		$fileName = $this->name;
		$path = $uploadFolder.$fileName;
		$thumbpath = $thumbFolder.$fileName;
		$altpath = $altFolder.$fileName;
			
		//copy the file in
		if (copy($file['tmp_name'], $path)){ //IF IT HAS BEEN COPIED... 
			
			//store the uploaded file path
			$this->filePath=$path;	
				
			//see if the file is a video
			if (substr_count(strtolower($path), ".mov"){
				$this->mediaType = 'video';
				
				if ($this->createThumb){
					$this->thumbPath = $thumbFolder."videothumb.png";
				}
			}
			//its a picture
			else{	
				$this->mediaType = 'image';
				
				if ($this->createAlt){
					resize_image_to($path, $altpath, 400);
					$this->altPath=$altpath;
				}
				
				if ($this->createThumb){					
					create_square_image($path,$thumbpath,75);
					$this->thumbPath=$thumbpath;
				}
			}
			
			//it has been uploaded
			return true;
		}
	}
}

class Upload{
	public $success = false;
	public $filecount = 0;
	public $files = array();
	
	function processUploads(){
		//what folder to save uploads into
		//$uploaddir = 
		//$thumbdir = 

		//see how many files we have
		$fileCount = $_POST['fileCount'];
		
		$filesCompleted=0;
		//loop over each file
		for ($i=0; $i<$fileCount; $i++){
		
			//get the file
			$postedName = 'uploadedFile' . $i;			
			$file = $_FILES[$postedName];
			
			//create the file
			$f = new UploadedFile($file);
			
			//process it 
			if ($f->processUpload()){
				$this->files[$filesCompleted] = $f;
				$filesCompleted++;
			}
		}

		$this->filecount = $filesCompleted;
		
		//see if the number of files sent = number of files downloaded
		// output simple string so the app knows the status
		if (($fileCount==0)||($this->filecount != $filesCompleted)){
			echo "Upload failed";
			$this->success =  false;
		}
		else{
			echo "Upload successful";
			$this->success = true;
		}
	}
}

	function resize_image_to($file, $tofile, $max_size = 200){
		// get width and height of original image
		$imagedata = getimagesize($file);
		$original_width = $imagedata[0];	
		$original_height = $imagedata[1];
		
		if($original_width > $original_height){
			$new_height = $max_size;
			$new_width = $new_height*($original_width/$original_height);
		}
		if($original_height > $original_width){
			$new_width = $max_size;
			$new_height = $new_width*($original_height/$original_width);
		}
		
		$new_width = round($new_width);
		$new_height = round($new_height);
		
		// load the image
		if(substr_count(strtolower($file), ".jpg") or substr_count(strtolower($file), ".jpeg")){
			$original_image = imagecreatefromjpeg($file);
		}
		if(substr_count(strtolower($file), ".gif")){
			$original_image = imagecreatefromgif($file);
		}
		if(substr_count(strtolower($file), ".png")){
			$original_image = imagecreatefrompng($file);
		}
		
		$smaller_image = imagecreatetruecolor($new_width, $new_height);
		
		imagecopyresampled($smaller_image, $original_image, 0, 0, 0, 0, $new_width, $new_height, $original_width, $original_height);
		
		// save the smaller image FILE if destination file given
		if(substr_count(strtolower($file), ".jpg")){
			imagejpeg($smaller_image,$tofile,100);
		}
		if(substr_count(strtolower($file), ".gif")){
			imagegif($smaller_image,$tofile);
		}
		if(substr_count(strtolower($file), ".png")){
			imagepng($smaller_image,$tofile,9);
		}

		imagedestroy($original_image);
		imagedestroy($smaller_image);
		
	}

	function create_square_image($original_file, $destination_file=NULL, $square_size = 96){		
		if(isset($destination_file) and $destination_file!=NULL){
			if(!is_writable($destination_file)){
				echo '<p style="color:#FF0000">Oops, the destination path is not writable. Make that file or its parent folder wirtable.</p>'; 
			}
		}
		
		// get width and height of original image
		$imagedata = getimagesize($original_file);
		$original_width = $imagedata[0];	
		$original_height = $imagedata[1];
		
		if($original_width > $original_height){
			$new_height = $square_size;
			$new_width = $new_height*($original_width/$original_height);
		}
		if($original_height > $original_width){
			$new_width = $square_size;
			$new_height = $new_width*($original_height/$original_width);
		}
		if($original_height == $original_width){
			$new_width = $square_size;
			$new_height = $square_size;
		}
		
		$new_width = round($new_width);
		$new_height = round($new_height);
		
		// load the image
		if(substr_count(strtolower($original_file), ".jpg") or substr_count(strtolower($original_file), ".jpeg")){
			$original_image = imagecreatefromjpeg($original_file);
		}
		if(substr_count(strtolower($original_file), ".gif")){
			$original_image = imagecreatefromgif($original_file);
		}
		if(substr_count(strtolower($original_file), ".png")){
			$original_image = imagecreatefrompng($original_file);
		}
		
		$smaller_image = imagecreatetruecolor($new_width, $new_height);
		$square_image = imagecreatetruecolor($square_size, $square_size);
		
		imagecopyresampled($smaller_image, $original_image, 0, 0, 0, 0, $new_width, $new_height, $original_width, $original_height);
		
		if($new_width>$new_height){
			$difference = $new_width-$new_height;
			$half_difference =  round($difference/2);
			imagecopyresampled($square_image, $smaller_image, 0-$half_difference+1, 0, 0, 0, $square_size+$difference, $square_size, $new_width, $new_height);
		}
		if($new_height>$new_width){
			$difference = $new_height-$new_width;
			$half_difference =  round($difference/2);
			imagecopyresampled($square_image, $smaller_image, 0, 0-$half_difference+1, 0, 0, $square_size, $square_size+$difference, $new_width, $new_height);
		}
		if($new_height == $new_width){
			imagecopyresampled($square_image, $smaller_image, 0, 0, 0, 0, $square_size, $square_size, $new_width, $new_height);
		}
		

		// if no destination file was given then display a png		
		if(!$destination_file){
			imagepng($square_image,NULL,9);
		}
		
		// save the smaller image FILE if destination file given
		if(substr_count(strtolower($destination_file), ".jpg")){
			imagejpeg($square_image,$destination_file,100);
		}
		if(substr_count(strtolower($destination_file), ".gif")){
			imagegif($square_image,$destination_file);
		}
		if(substr_count(strtolower($destination_file), ".png")){
			imagepng($square_image,$destination_file,9);
		}

		imagedestroy($original_image);
		imagedestroy($smaller_image);
		imagedestroy($square_image);

	}

?>