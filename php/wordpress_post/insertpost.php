<?php
	//constants
	$STATUS_PENDING = 'pending';
	$STATUS_PUBLISH = 'publish';
	
	//settings
	$post_status = $STATUS_PUBLISH;	

	error_reporting(E_ALL);
	ini_set('display_errors', '1');
	
	// includes	
	require_once($_SERVER['DOCUMENT_ROOT'].'/wp-load.php');
	require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-admin/includes/media.php');
	require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-admin/includes/file.php');
	require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-admin/includes/image.php');
	require_once("mediapostinc.php");
    
	
	/*
	$post = array(
	  'ID' => [ <post id> ] //Are you updating an existing post?
	  'menu_order' => [ <order> ] //If new post is a page, sets the order should it appear in the tabs.
	  'comment_status' => [ 'closed' | 'open' ] // 'closed' means no comments.
	  'ping_status' => [ 'closed' | 'open' ] // 'closed' means pingbacks or trackbacks turned off
	  'pinged' => [ ? ] //?
	  'post_author' => [ <user ID> ] //The user ID number of the author.
	  'post_category' => [ array(<category id>, <...>) ] //Add some categories.
	  'post_content' => [ <the text of the post> ] //The full text of the post.
	  'post_date' => [ Y-m-d H:i:s ] //The time post was made.
	  'post_date_gmt' => [ Y-m-d H:i:s ] //The time post was made, in GMT.
	  'post_excerpt' => [ <an excerpt> ] //For all your post excerpt needs.
	  'post_name' => [ <the name> ] // The name (slug) for your post
	  'post_parent' => [ <post ID> ] //Sets the parent of the new post.
	  'post_password' => [ ? ] //password for post?
	  'post_status' => [ 'draft' | 'publish' | 'pending'| 'future' | 'private' ] //Set the status of the new post. 
	  'post_title' => [ <the title> ] //The title of your post.
	  'post_type' => [ 'post' | 'page' | 'link' | 'nav_menu_item' | custom post type ] //You may want to insert a regular post, page, link, a menu item or some custom post type
	  'tags_input' => [ '<tag>, <tag>, <...>' ] //For tags.
	  'to_ping' => [ ? ] //?
	  'tax_input' => [ array( 'taxonomy_name' => array( 'term', 'term2', 'term3' ) ) ] // support for custom taxonomies. 
	);  */
	
	$upload = new Upload();
	$upload->processUploads();
	
	// if everything uploaded successfully, let's continue
	if ($upload->success){
		
		//process each item and build the post
		$content = "";
		for($i=0;$i<$upload->filecount;$i++){
			//$fileString="<div class=\"slide\"><img src=\"$uploadedFiles[$i]\" width=\"920\" height=\"400\" alt=\"side\" /></div>";
			if ($upload->files[$i]->mediaType='image'){
				$content .= '<img align="center" src="'. $upload->files[$i]->thumbPath .'" />';
			}
			else if ($upload->files[$i]->mediaType='video'){
				$content .= '<video align="center" src="'. $upload->files[$i]->thumbPath .'" />';
			}
		}			
		//$imgpath = "../../wp-content/uploads/2012/07/calcsteth.png";
		
		// Create post object
		 $my_post = array(
			 'post_title' => 'Mobile Upload',
			 'post_content' => $content,
			 'post_status' => 'pending',
			 'post_author' => 1,
			 'post_category' => array(8)
		 );

		// Add the post
		$cur_post_id = wp_insert_post( $my_post );
		
		// add the first file as the thumbnail
		$imgpath = $upload->files[0]->thumbPath;
		$wp_filetype = wp_check_filetype(basename($imgpath), null );
		$attachment = array(
			'post_mime_type' => $wp_filetype['type'],
			'post_title' => preg_replace('/\.[^.]+$/', '', basename($imgpath)),
			'post_content' => '',
			'post_status' => 'inherit'
		);
		$attach_id = wp_insert_attachment( $attachment, $imgpath, $cur_post_id );
		add_post_meta($cur_post_id, '_thumbnail_id', $attach_id, true);
	}
?>