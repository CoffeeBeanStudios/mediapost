For integration into a PHP project, download mediapostinc.php into a folder on the server.

Create a php page to process your uploads then add the following:

include "MediaPostInc.php";
$settings = Array();
$upload = new Upload($settings);
$upload->processUploads();

This alone will receive the files into the /uploads folder on your web server.

To do something with each file after they're uploaded add the following:

if ($upload->success){
	for($i=0;$i<$upload->filecount;$i++){
		if ($upload->files[$i]->mediaType='image'){
			//process image file	
			// $upload->files[$i]->filePath
		}
		else if ($upload->files[$i]->mediaType='video'){
			//process video file	
			//  $upload->files[$i]->filePath
		}

	}
}

Settings

To configure the uploader you can override the following defaults:
	//upload folder
	settings['uploadFolder'] = "uploads/";	

	//settings for thumbnail creation

	settings['createThumb'] = true;
		
	settings['thumbFolder'] = "uploads/thumbs/";
		
	settings['thumbSize'] = 75;
						
			

	//alternate size image creation
		
	settings['createAlt'] = true;
		
	settings['altFolder'] = "uploads/alt/";
			
	settings['altSize'] = 400;
